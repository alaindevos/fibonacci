function f = fibonacci(n)
	select n
	case 0 then
				f = 0
	case 1 then
				f = 1
	else
				f = fibonacci(n-1)+fibonacci(n-2)
	end
endfunction
disp(fibonacci(35))
quit()
