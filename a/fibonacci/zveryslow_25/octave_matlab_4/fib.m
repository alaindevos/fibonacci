#!/usr/local/bin/octave -qf

# example octave script

arg_list = argv ();
num = str2num(arg_list{1});

function f=fib(n)
  if (n<2)
    f=n;
  else
    f=fib(n-1)+fib(n-2);
  endif
endfunction

printf("%d",fib(num));
