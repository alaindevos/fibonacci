int fibonacci(int n){
    if (n < 2) {
        return n;
    } else {
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}

stdout.printf(fibonacci(42).to_string());
