package alain

import scala.Array

object CubeCalculator:
  def cube(x: Int): Int = x * x * x

class Fibo(_a: Int):
  val a: Int = _a
  def fibonacci(): Int =
    // @annotation.tailrec : not tailrecursive
    def calc(x: Int): Int =
      if x <= 2 then 1 else calc(x - 1) + calc(x - 2)
    calc(a)

@main private def main(args: String*): Int =
  var myArgs: Seq[String] = args
  //myArgs = Seq("5")
  val firstString: String = myArgs.head
  val first: Int = firstString.toInt
  val f = Fibo(first)
  val result: Int = f.fibonacci()
  println("Calculation fibonacci of : " + firstString)
  println("Result : " + result)
  0

