name := "fiboApp"
version := "1"
lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "alain",
      scalaVersion := "3.3.3"
    )),
    name := "test"
  )

libraryDependencies ++= Seq("org.scalatest" %% "scalatest" % "3.2.19" % Test)
