// Generated by Haxe 4.3.4
#ifndef INCLUDED_MyFibo
#define INCLUDED_MyFibo

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_STACK_FRAME(_hx_pos_63f08806a273a874_10_new)
HX_DECLARE_CLASS0(MyFibo)



class HXCPP_CLASS_ATTRIBUTES MyFibo_obj : public ::hx::Object
{
	public:
		typedef ::hx::Object super;
		typedef MyFibo_obj OBJ_;
		MyFibo_obj();

	public:
		enum { _hx_ClassId = 0x04d6ae3c };

		void __construct(int _myin);
		inline void *operator new(size_t inSize, bool inContainer=false,const char *inName="MyFibo")
			{ return ::hx::Object::operator new(inSize,inContainer,inName); }
		inline void *operator new(size_t inSize, int extra)
			{ return ::hx::Object::operator new(inSize+extra,false,"MyFibo"); }

		inline static ::hx::ObjectPtr< MyFibo_obj > __new(int _myin) {
			::hx::ObjectPtr< MyFibo_obj > __this = new MyFibo_obj();
			__this->__construct(_myin);
			return __this;
		}

		inline static ::hx::ObjectPtr< MyFibo_obj > __alloc(::hx::Ctx *_hx_ctx,int _myin) {
			MyFibo_obj *__this = (MyFibo_obj*)(::hx::Ctx::alloc(_hx_ctx, sizeof(MyFibo_obj), false, "MyFibo"));
			*(void **)__this = MyFibo_obj::_hx_vtable;
{
            	HX_STACKFRAME(&_hx_pos_63f08806a273a874_10_new)
HXDLIN(  10)		( ( ::MyFibo)(__this) )->myin = _myin;
            	}
		
			return __this;
		}

		static void * _hx_vtable;
		static Dynamic __CreateEmpty();
		static Dynamic __Create(::hx::DynamicArray inArgs);
		//~MyFibo_obj();

		HX_DO_RTTI_ALL;
		::hx::Val __Field(const ::String &inString, ::hx::PropertyAccess inCallProp);
		::hx::Val __SetField(const ::String &inString,const ::hx::Val &inValue, ::hx::PropertyAccess inCallProp);
		void __GetFields(Array< ::String> &outFields);
		static void __register();
		bool _hx_isInstanceOf(int inClassId);
		::String __ToString() const { return HX_("MyFibo",7c,eb,8b,a3); }

		int myin;
		int calc(int x);
		::Dynamic calc_dyn();

		int fibo();
		::Dynamic fibo_dyn();

};


#endif /* INCLUDED_MyFibo */ 
