#lang typed/racket
(define (fib [n : Integer]) : Integer
  (if (< n 2)
      n
      (+ (fib(- n 1)) (fib(- n 2)))
  )
)
(display (fib 42))
