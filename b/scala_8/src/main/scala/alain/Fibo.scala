package alain

import scala.Array

object CubeCalculator:
  def cube(x: Int): Int = x * x * x

abstract class FiboAbstract(_a: Int) :
  val a: Int 
  def fibonacci(): Int

class Fibo(_a: Int) extends FiboAbstract(_a:Int) :
  val a: Int = _a

  def fibonacci(): Int = 
    def calc(x: Int): Int = 
      if x <= 2 then 1 else calc(x - 1) + calc(x - 2)
    calc(a)
  

@main private def main(args: String*): Int = 
  var myArgs: Seq[String] = args
  // myArgs = Seq("5")
  val firstString: String = myArgs.head
  val first: Int = (augmentString(firstString)).toInt
  val f:Fibo = Fibo(first)
  val result: Int = f.fibonacci()
  println("Calculation fibonacci of : " + firstString)
  println("Result : " + result)
  0

