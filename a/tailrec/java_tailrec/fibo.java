class fibo{
    public static void main (String[] args){
        System.out.println(fib(42,0,1) );
    }
    static int fib(int n, int a, int b ){
        if (n==0) return a;
        else if (n==1) return b;
        else return fib(n - 1, b, a + b);
    }
}
