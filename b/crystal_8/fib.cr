class Fibonacci
  def initialize(_in : Int32)
    @in=_in
  end

  def fib(n : Int32)
    if n <= 2
      1
    else 
      fib(n - 1) + fib(n - 2)
    end
  end

  def calc()
    return fib(@in) 
  end 
end

f=Fibonacci.new(46)
puts f.calc()
