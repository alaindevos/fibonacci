#include "stdio.h"
#include <stdlib.h>

int fib(long n){
  if (n < 2) return n;
  else return fib(n-1) + fib(n-2);
}

int main(int argc, char *argv[]){
//char *p=NULL;
//long in=strtol(argv[1],&p,10);
printf("%d\n", fib(45));
return 0;
}

