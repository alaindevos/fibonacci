(declaim
  (ftype
    (function (fixnum) fixnum) fib))

(defun fib (n)
  (if (<= n 2)
      1
      (+ (fib(- n 1)) (fib(- n 2)))))

(defun main ()
	(write (fib 46)))

(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
