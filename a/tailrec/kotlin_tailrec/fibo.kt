@JvmOverloads
tailrec fun fibo(n: Int, a: Int = 0, b: Int = 1): Int =
    when (n) {
        0 -> a
        1 -> b
        else -> fibo(n - 1, b, a + b)
    }

fun main(args: Array<String>) {
val x = 42
val y = fibo(x)
print("$x \n")
print("$y \n")
}
