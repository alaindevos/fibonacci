(define (fib n ::long) ::long
  (if (< n 2)
      n
      (+ (fib(- n 1)) (fib(- n 2)))
  )
)
(display (fib 42))
