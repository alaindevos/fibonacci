import scalafx.application.JFXApp3
import scalafx.scene.Scene
import scalafx.scene.layout.HBox
import scalafx.Includes._
import scalafx.scene.control._
import scalafx.event.ActionEvent

def fibonacci(a:Int ): Int =
  def calc(x: Int): Int =
    if x <= 2 then 1 else calc(x - 1) + calc(x - 2)
  calc(a)
object MyProgram extends JFXApp3 {
  override def start(): Unit = {
    stage = new JFXApp3.PrimaryStage {
      title = "MyProgram"
      scene = new Scene(400,400) {
        //fill = Color.rgb(38, 38, 38)
        var textField = new TextField
        textField.layoutX = 20
        textField.layoutY = 20
        val button = new Button("Calculate")
        button.layoutX = 20
        button.layoutY = 50
        val label = new Label("MyLabel")
        label.layoutX = 20
        label.layoutY = 80
        button.onAction = (e:ActionEvent) => {
          val myStringInput:String = textField.getText()
          val input = myStringInput.toInt
          val output = fibonacci(input)
          val myStringOutput = output.toString
          label.setText(myStringOutput)
        }
        content = List(textField,button, label)
      }}}}
