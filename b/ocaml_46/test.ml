let _ =
    let rec fib (n:int):int =
        if n <= 2 
            then 1
            else fib (n-2)+fib (n-1) in
    Printf.printf "%d\n" (fib 46)  ; 
    ()
