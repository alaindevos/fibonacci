Integer fib(Integer n){
	if(n==0||n==1){ return n; }
	return fib(n-1)+fib(n-2);
}

shared void main(){
	value n=45;
	print("fib(``n``) = ``fib(n)``");
}
