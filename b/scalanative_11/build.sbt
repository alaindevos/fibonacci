scalaVersion := "3.3.3" // A Long Term Support version.
organization := "alain"
name := "test"
version := "1"

enablePlugins(ScalaNativePlugin)

// set to Debug for compilation details (Info is default)
logLevel := Level.Info

import scala.scalanative.build._

// defaults set with common options shown
nativeConfig ~= { c =>
  c.withLTO(LTO.none) // thin
    .withMode(Mode.debug) // releaseFast
    .withGC(GC.immix) // commix
}

import scala.collection.immutable.Seq

libraryDependencies ++= Seq("org.scalatest" %% "scalatest" % "3.2.19" % Test)
