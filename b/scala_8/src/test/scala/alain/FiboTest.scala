package alain

class CubeCalculatorTest extends org.scalatest.funsuite.AnyFunSuite :
  test("CubeCalculator.cube") :
    assert(CubeCalculator.cube(3) === 27)
  
class FiboTest extends org.scalatest.funsuite.AnyFunSuite :
  test("Fibo") :
    val testVal=5
    val expectedTestResult=8
    val f=new Fibo(testVal)
    val testResult=f.fibonacci()
    assert(testResult == expectedTestResult)
