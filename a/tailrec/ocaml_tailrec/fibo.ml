let _ =
  let fib (n : int) : int =
    let rec myloop (i : int) (a : int) (b : int) : int =
      if i = n then a else myloop (i + 1) b (a + b)
    in
    myloop 0 0 1
  in
  let () = Printf.printf "%d\n" (fib 42) in
  ()
