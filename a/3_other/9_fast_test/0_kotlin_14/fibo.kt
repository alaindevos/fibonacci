fun fibo(x: Long):Long{
	if (x<2) return x
	else return (fibo(x-1)+fibo(x-2))
}

fun main(args: Array<String>) {
val x = 47L
val y = fibo(x)
print("$x \n")
print("$y \n")
}
