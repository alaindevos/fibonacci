/**
    Multi-line comments for documentation.
**/
// Single line comment
import Sys.println ;

class MyFibo {
    public var myin:Int;
    public function new(_myin:Int){
        myin=_myin;
    }//new
 
    public function calc(x:Int):Int {
        if(x<=2){
            return 1 ;
        }
        else return (calc(x-1)+calc(x-2)) ;
    }

    public function fibo():Int {
        return calc(myin);   
    }
}

 
class Test {
    public static function main():Void {
        var name:String=Sys.args()[0];
        //trace(name);
        var cfibo:MyFibo = new MyFibo(46);
        var y=cfibo.fibo();
        trace(y);
        }
}//Main

