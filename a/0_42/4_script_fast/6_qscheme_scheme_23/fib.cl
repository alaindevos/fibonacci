#!/usr/local/bin/sbcl --script
(defun fib (n)
  (if (< n 2)
      n
      (+ (fib(- n 1)) (fib(- n 2)))
  )
)
(write (fib 42))
