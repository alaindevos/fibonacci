// recursive

let rec fiboRec =
  function
  | 0 -> 0
  | 1 -> 1
  | n -> fiboRec (n-1) + fiboRec (n-2)

printfn "%d" (fiboRec 42)
