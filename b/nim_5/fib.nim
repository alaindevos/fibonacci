type
  TFibo = object
    input : int32 

proc CalcFibo(cf : TFibo , n : int32) : int32 =
  result = (int32) 0
  if n <= 2:
    result = 1
  else:
    result = cf.CalcFibo(n - 1) + cf.CalcFibo(n - 2)
  return result 

proc Fibonacci(tf : TFibo) : int32 =
  tf.CalcFibo(tf.input)

var x = (int32) 46
var tf : TFibo = TFibo(input : x)
echo tf.Fibonacci()
