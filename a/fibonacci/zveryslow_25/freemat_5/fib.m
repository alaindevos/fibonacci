function [f]=fib(a)
if (a<2) 
	f=a;
else 
	f=fib(a-1)+fib(a-2);
end
