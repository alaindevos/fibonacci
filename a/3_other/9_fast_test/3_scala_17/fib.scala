object fib {
	def fibo(n: Long): Long =
		if (n < 2) n
		else fibo(n - 1) + fibo(n - 2);
	def main(args: Array[String]): Unit = {
		Console.println("fib("+ 47 +") = " + fibo(47));
    }
}
