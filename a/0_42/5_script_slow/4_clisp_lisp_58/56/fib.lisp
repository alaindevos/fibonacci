(defun fib(n)
  (cond
    ((eq n 0) 0)
    ((eq n 1) 1)
    ((+ (fib (- n 1)) (fib (- n 2))))
  )
)
(write (fib 42));
