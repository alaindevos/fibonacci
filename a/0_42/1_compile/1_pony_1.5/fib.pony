actor Main
  fun fib(n: U32): U32 =>
    if n < 2 then
      n
    else
      fib(n - 1) + fib(n - 2)
    end

  new create(env: Env) =>
    env.out.print(fib(42).string())

