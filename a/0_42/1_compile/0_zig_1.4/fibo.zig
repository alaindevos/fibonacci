const std = @import("std");
const warn = std.debug.warn;
const os = std.os;
const assert = std.debug.assert;

fn fibo(x: i64) i64 {
    if (x < 2) {
        return x;
    }
    else{
    return (fibo(x-1)+fibo(x-2));
    }
}
pub fn main() void {
var x: i64 = 42;
warn ("{}\n",x);
warn ("{}\n",fibo(x));
}
