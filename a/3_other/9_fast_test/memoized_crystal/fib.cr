class Global
  class_property cache = {0_i64 => 0_i64 , 1_i64 => 1_i64 }
end

def fib(n : Int64)
  return 1_i64  if n < 2
  return Global.cache[n] if Global.cache[n]?
  Global.cache[n] = fib(n - 1) + fib(n - 2)
end

puts fib(47)
