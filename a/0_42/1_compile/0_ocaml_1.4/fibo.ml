let _ =
  let rec fib (n:int):int =
    if n < 2 then n
             else fib (n-2)+fib (n-1) in
  let () = Printf.printf "%d\n" (fib 42) in
  ()
