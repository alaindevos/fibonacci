with Ada.Text_IO, Ada.Command_Line;
procedure Fib is
   function Fib (P : Positive) return Positive;
   function Fib (P : Positive) return Positive is
   begin
      if P <= 2 then
         return 1;
      else
         return Fib (P - 1) + Fib (P - 2);
      end if;
   end Fib;
begin
   Ada.Text_IO.Put_Line (Integer'Image (Fib (42)));
end Fib;

