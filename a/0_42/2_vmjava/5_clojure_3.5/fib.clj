(defn fib ^long [^long n]
 (case n
   0 0
   1 1
   (+ (fib (- n 1)) (fib (- n 2)))
 )
)
(printf "%d" (fib 42))
