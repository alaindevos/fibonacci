
fn fibonacci(x: i32) -> i32 {
  if x < 2 {
    return x;
  } else {
    return fibonacci(x - 1) + fibonacci(x - 2);
  }
}

fn main() {
    println!("{}", fibonacci(42));
}
