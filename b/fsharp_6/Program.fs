﻿//open System
open Xunit

[<AbstractClass>]
type AbstractFibo(_a: int) =
    abstract member a: int
    abstract member fibo: int -> int
    abstract member fibonacci: Unit -> int

type Fibo(_a: int) =
    inherit AbstractFibo(_a)
    override this.a = _a

    override this.fibo(x: int) : int =
        if (x <= 2) then
            1
        else
            this.fibo (x - 1) + this.fibo (x - 2)

    override this.fibonacci() : int = this.fibo (this.a)

[<Fact>]
let FiboTest () =
    let expected: int = 8
    let actual: int = Fibo(5).fibonacci ()
    Assert.Equal(expected, actual)



[<EntryPoint>]
let main (args: string array) : int =
    let mutable x: int = 0
    x <- args[0] |> int
    //  x <- 5
    printfn "Calculation fibonnaci of : %d" x
    let c: Fibo = Fibo(x)
    let result: int = c.fibonacci ()
    printfn "Result : %d" result

    0
